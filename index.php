<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

// Autoloader
require 'vendor/autoload.php';

use Akrillia\Routing\Route;
use Akrillia\Routing\Router;
use Akrillia\Routing\RouteCollection;

include __DIR__ . '/Akrillia/Routing/Route.php';
include __DIR__ . '/Akrillia/Routing/RouteCollection.php';
include __DIR__ . '/Akrillia/Routing/Router.php';

try {
    $route4 = new Route('/test/bat/{bar}/{bar?}', 'TestController:bar');
    $route3 = new Route('/test/bar/{bat}', 'TestController:bat');
    $route2 = new Route('/test/foo', 'TestController:foo');
    $route1 = new Route('/test/', 'TestController:test');

    $rc = new RouteCollection;
    $rc->addRoute($route4, 'test2');
    $rc->addRoute($route3, 'test');
    $rc->addRoute($route2, 'foo');
    $rc->addRoute($route1, 'bar');
    $router = new Router($rc);
    var_dump($router->match('/test/')); // true
    var_dump($router->match('/test/foo')); // true
    var_dump($router->match('/test/bla')); // false
    var_dump($router->match('/test/bar/bla')); // true
    var_dump($router->match('/test/bar/bla/bli')); //false
    var_dump($router->match('/test/bat/test')); //true
    var_dump($router->match('/test/bat/test/test')); //true und so
} catch(Exception $e) {
    echo $e->getMessage();
} catch(ErrorException $ex) {
    echo $ex->getMessage();
}
