<?php

namespace Akrillia\Routing;

class Route
{
    private $path = null;

    private $pattern = null;

    private $callable = null;

    private $options = [];

    public function __construct(string $path, $callable, array $options = [])
    {
        $this->path = $path;
        $this->pattern = $this->createPattern($path);

        if(is_callable($callable) || is_string($callable)) {
            $this->callable = $callable;
        } else {
            throw new Exception('not callable');
        }

        $this->options = $options;
    }

    public function getPattern()
    {
        return $this->pattern;
    }

    protected function createPattern(string $path) : string
    {
        $path = str_replace('/', '\/', $path);
        $new = null;
        if(false !== strpos($path, '{')) {
            preg_match('$\{[a-zA-Z0-9]{1,}\}$', $path, $required);
            foreach($required as $value) {
                $path = str_replace($value, '[a-zA-Z0-9]{1,}', $path);
            }

            preg_match('$\{[a-zA-Z0-9]{1,}[?]\}$', $path, $optional);
            foreach($optional as $value) {
                $path = str_replace($value, '[a-zA-Z0-9]{0,}', $path);
            }
        }

        return '$'.$path.'$';
    }
}
