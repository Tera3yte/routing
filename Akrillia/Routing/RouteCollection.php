<?php

namespace AkrilliA\Routing;

use AkrilliA\Routing\Route;

class RouteCollection
{
    private $routes = [];

    public function addRoute(Route $route, string $name = null) : RouteCollection
    {
        if(!is_null($name)) {
            $this->routes[$name] = $route;
        } else {
            $this->routes[] = $route;
        }

        return $this;
    }

    public function getRoutes()
    {
        return $this->routes;
    }
}
