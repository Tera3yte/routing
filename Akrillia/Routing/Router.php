<?php

namespace AkrilliA\Routing;

use AkrilliA\Routing\RouteCollection;

class Router
{
    private $routeCollection = null;

    public function __construct(RouteCollection $routeCollection)
    {
        $this->routeCollection = $routeCollection;
    }

    public function match(string $path)
    {
        $routes = $this->routeCollection->getRoutes();

        $last = false;
        foreach($routes as $route) {
            echo 'Matchable: '.$route->getPattern().'<br>';
            echo 'Path: '.$path.'<br>';
            echo '<hr>';
            if(preg_match($route->getPattern(), $path)) {
                return true;
            } else {
                //return $last;
                //echo $route->getMatchable() . ' -- ' . $path . ' <br><br>';
            }
        }

        return $last;

        //throw new \Exception('route not found');
    }
}
